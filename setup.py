from distutils.core import setup

setup(name = 'Calculator',
author = 'Rene Angeles',
author_email = 'reneang17@gmail.com',
url = 'https://github.com/reneang17/Calculator',
packages = ['Calculator']
)

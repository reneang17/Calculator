import numpy as np
import numpy.testing as npt

import Calculator

def test_Calculator_smoke():
    #Smoke_test
    obt = Calculator.Calculator_object()

def test_Calculator_object_fizz():
    #test the fizz_function
    obj = Calculator.Calculator_object()
    output = obj.fizz()

    npt.assert_equal(output, "buzz")
